import { Component, OnInit } from '@angular/core';
import { getUsers } from '@src/api/users'
import { MatDialog } from '@angular/material';
import { AddDialogComponent } from './add-dialog/add-dialog.component';
import { EditDialogComponent } from './edit-dialog/edit-dialog.component';
import { ShowDialogComponent } from './show-dialog/show-dialog.component';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {
  users: any[]
  displayedColumns: String[]
  updated: Boolean

  constructor(public dialog : MatDialog) {
    this.fetchUsers()
    this.displayedColumns = ['id', 'name', 'email', 'role', 'actions']
    this.updated = true
  }

  async fetchUsers () {
    try {
      let fetchedUsers = await getUsers()
      this.users = fetchedUsers.data.filter(user => user.role === 'user')
    } catch (error) {
      console.log(`It seems like an error occured !\nHere is the error's details \nError: ${error.message}`)
      console.log(error)
    }
    // getUsers()
    //   .then((response) => {
    //     this.users = response.data
    //   })
    //   .catch((error) => {
    //     console.log(`It seems like an error occured !\nHere is the error's details \nError: ${error.message}`)
    //     console.log(error)
    //   })
  }

  openAddDialog () {
    let dialogRef = this.dialog.open(AddDialogComponent)

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.refreshTable()
        let lastId = this.users[this.users.length - 1].id + 1
        this.users.push({id: lastId, ...result})
      }
    })
  }

  openEditDialog (user) {

    let dialogRef = this.dialog.open(EditDialogComponent, {
      data: user
    })

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.refreshTable()
        this.users[this.users.indexOf(user)] = result
      }
    })
  }
  refreshTable() {
    this.updated = false
    setTimeout(() => {
      this.updated = true
    }, 5)
  }

  openShowDialog (user) {

    let dialogRef = this.dialog.open(ShowDialogComponent, {
      data: user
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('Shpw Dialog Closed')
    })
  }

  openDeleteDialog (user) {

    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: user
    })

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.refreshTable()
        this.users = this.users.filter(user => user.id !== result.id)
        // this.users.splice(this.users[this.users.indexOf(result)], 1)
      }
      console.log('Delete Dialog Closed')
    })
  }
  ngOnInit() {
  }

}
