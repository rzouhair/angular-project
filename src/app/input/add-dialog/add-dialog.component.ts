import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { validEmail, validRole } from '@src/utils/validate'
import { insertUser } from '@src/api/users'

@Component({
  selector: 'app-add-dialog',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.scss']
})
export class AddDialogComponent implements OnInit {
  user: any
  roles: String[]
  error: String
  constructor(
    public dialogRef: MatDialogRef<AddDialogComponent>) {
      this.user = {
        name: '',
        email: '',
        password: '',
        role: ''
      }
      this.roles = ['admin', 'user', 'provider']
    }
  async confirmUserAdd () {
    if(validEmail(this.user.email) && validRole(this.user.role) && this.user.name !== '') {
      console.log('Submit !!!!')
    }
    let errList = []
    if(!validEmail(this.user.email)) {
      let errMsg = `Please enter a valid email`
      errList.push(errMsg)
    }
    if(!validRole(this.user.role)) {
      let errMsg = `Please choose a role from the list`
      errList.push(errMsg)
    }
    if(this.user.name === '') {
      let errMsg = `Please enter a name`
      errList.push(errMsg)
    }
    if(this.user.name === '') {
      let errMsg = `Please enter a name`
      errList.push(errMsg)
    }
    if(this.user.password === '') {
      let errMsg = `Please enter a valid password`
      errList.push(errMsg)
    }
    errList.forEach(error => {
      this.error = this.error === '' || this.error ? `${this.error}\n${error}` : error
      setTimeout(() => {
        this.error = ''
      }, 2500)
    })

    if(errList.length === 0) {
      try {
        await insertUser(this.user)
        this.dialogRef.close(this.user)
      } catch (error) {
        console.log(error)
      }
    }
  }
  ngOnInit() {
  }

}
