import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { updateUser } from '@src/api/users'
import { EquivalentService } from '@src/app/equivalent.service'

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.scss']
})
export class EditDialogComponent implements OnInit {
  user: any
  oldUser: Object
  roles: String[]
  equiv: EquivalentService
  error: String
  constructor(public dialogRef: MatDialogRef<EditDialogComponent>, @Inject(MAT_DIALOG_DATA) public data, public equivalentService: EquivalentService) {
    this.user = {...data}
    this.oldUser = data
    this.equiv = equivalentService
    this.roles = ['admin', 'user', 'provider']
  }

  async confirmUserEdit(){
    if(!this.equiv.isEquivalent(this.user, this.oldUser)) {
      try {
        if(this.user.hasOwnProperty('id')) {
          await updateUser(this.user.id, this.user)
          this.oldUser = this.user
          this.dialogRef.close(this.user);
        }
      } catch (error) {
        console.log(error)
      }

    }else {
      this.error = 'Cannot be updated'
      setTimeout(() => {
        this.error = ''
      }, 3000)
    }
  }

  ngOnInit() {
  }

}
