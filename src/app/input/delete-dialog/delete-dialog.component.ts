import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { destoryUser } from '@src/api/users'
@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss']
})
export class DeleteDialogComponent implements OnInit {
  user: any
  constructor(
    public dialogRef: MatDialogRef<DeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
     this.user = data
    }

  ngOnInit() {
    // console.log(this.user)
  }

  async confirmUserDelete() {
    try {
      console.log(this.user.id)
      await destoryUser(this.user.id)
      this.dialogRef.close(this.user)
      console.log('User deleted')
    } catch (error) {
      console.log(error)
    }
  }

}
