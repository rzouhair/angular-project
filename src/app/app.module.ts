import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InputComponent } from './input/input.component';
import { SecondComponent } from './second/second.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { AddDialogComponent } from './input/add-dialog/add-dialog.component';
import { EditDialogComponent } from './input/edit-dialog/edit-dialog.component';
import { ShowDialogComponent } from './input/show-dialog/show-dialog.component';
import { DeleteDialogComponent } from './input/delete-dialog/delete-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    InputComponent,
    SecondComponent,
    AddDialogComponent,
    EditDialogComponent,
    ShowDialogComponent,
    DeleteDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: InputComponent },
      { path: 'second', component: SecondComponent }
    ]),
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [AddDialogComponent, ShowDialogComponent, EditDialogComponent, DeleteDialogComponent]
})
export class AppModule { }
