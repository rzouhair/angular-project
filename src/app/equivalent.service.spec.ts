import { TestBed } from '@angular/core/testing';

import { EquivalentService } from './equivalent.service';

describe('EquivalentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EquivalentService = TestBed.get(EquivalentService);
    expect(service).toBeTruthy();
  });
});
