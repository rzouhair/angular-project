import request from '../utils/request'

export function getUsers() {
    return request({
        url: '/users',
        method: 'get'
    })
}

/**
 * This function stands for getting a specific user by ID
 * 
 * @param {Number} id 
 * @returns {Request}
 */

export function getUser(id) {
    return request({
        url: `/users/${id}`,
        method: 'get'
    })
}

/**
 * This function stands for updating a user
 * 
 * @param {Number} id 
 */

export function insertUser(params) {
    return request({
        url: `/users`,
        method: 'post',
        params
    })
}

/**
 * This function stands for updating a user
 * 
 * @param {Number} id 
 */

export function updateUser(id, params) {
    return request({
        url: `/users/${id}`,
        method: 'put',
        params
    })
}

/**
 * This function stands for destroying a user
 * 
 * @param {Number} id 
 */

export function destoryUser(id) {
    return request({
        url: `/users/${id}`,
        method: 'delete'
    })
}